Homework 3 Answers 
Shane Acoveno

1a.

all_characters = ? all visible characters ? ;
valid_characters = all_characters - " " " - " \ " ;
string = " " " , {valid_characters}, " " " ;

1b.

all_characters = ? all visible characters ? ;
valid_comment1 = " /* ", {all_characters}, " */ ";
valid_comment2 = " // ", {all_characters};
comment = "valid_comment1" | "valid_comment2";

1c.

valid_character = all_characters - " " - "@" 

1d.


1e.


2.
    1) An example of a lexical error, detected by the scanner is: println!('test'); because test is a str literal so it will not be assigned to a character literal.
    
    2) An example of a syntax error, detected by the parser is if there were two print statements with no semicolons after each: println("test1")
                                                                                                                                 println("test2")
      When I ran the above code, I was prompted with:  error: expected one of `.`, `;`, `?`, `}`, or an operator
   
    3) An example of a static semantic error, detected by semantic analysis is: let b: bool = 7; because it expects a bool however after executing it finds an integer type.

    4) An example of an ownership error, detected by the borrow checker:          let x = String::from("hello");
                                                                                  let y = x;
                                                                                  println!("{}", x);
      The above code yields the error "Use of moved value 'x' " because in Rust, when variables go out of scope, the memory is deallocated.

    5) An example of a lifetime error, detected through lifetime analysis of variables: 


3) In rust, the limit of the value on a usize is dependent upon the specific computer architecture one is running.  As an example, on a 32 bit x86 computer, usize =32, 
which has a max value of 4294967295. On a x86_64 usize =64, which has a max value of 18446744073709551615. In the event of an arithmetic overflow, I attempted to overflow a usize on my 64 bit computer and got the error "literal out of range for 'usize' ". The implications of size limits from one machine to another is that usize may work on a 64 bit machine yet may overflow on a 32 bit machine, suggesting that this is an issue in regards to portability. 

4) It may be difficult to tell if a program is correct or not because there may be edge cases in which a program does not work or contains a bug. Personally, I use a debugger to check the state of variables during different methods and steps within my code. Bugs that are revealed in testing are logical errors, in which an accepted value differs from the actual value.  Bugs that cannot be caught by testing are those outside of the testing coverage.

5) 
1.
root
| statement
| subroutine_call
  | id
    |"print"
  |"("
  |argument_list
    |expr
      |primary
        |id
          |"x"
  |")"

2.
root
| assignment
  |identifier
    |"y"
  |":="
  |expression
   |primary
    |identifier
      |"add"
      |"("
	
6.
1. G is able to check the balanced and non-balanced strings with the parentheses.

2.

G
    |G
        |N
            |"("
            |L
                |L
                    |e
                |"("
            |"]"
    |B
        |"("
        |E
            |e
        |")"



3.

G
    |G
        |G
        |B
            |"("
            |E
                |"("
                |E
                    |"("
                    |E
                        |e
                    |")"
                |")"
            |")"
        |e
